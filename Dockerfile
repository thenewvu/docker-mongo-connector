FROM python:3.5.2-slim
MAINTAINER thenewvu@gmail.com

ENV MONGO_HOST=mongo \
    MONGO_PORT=27017 \
    ES_HOST=elasticsearch \
    ES_PORT=9200

RUN pip install mongo-connector==2.4 \
                elastic2-doc-manager==0.2.0

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

COPY mongo /usr/bin/
RUN chmod +x /usr/bin/mongo

VOLUME /data

CMD /entrypoint.sh -v \
                   --tz-aware \
                   -m $MONGO_HOST:$MONGO_PORT \
                   -t $ES_HOST:$ES_PORT \
                   -d elastic2_doc_manager \
                   --auto-commit-interval=0
