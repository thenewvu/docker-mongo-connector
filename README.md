## What is this ?

A docker image that packages `mongo-connector` and its plugin `elastic2-doc-manager` with a goal is to provide a docker-based solution for synchronising data from a `mongo` node to a `elasticsearch` node.

## License

Read `LICENSE`.
