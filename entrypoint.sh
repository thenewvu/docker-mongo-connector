#!/bin/bash

####################################################################################################
# Eval a mongo shell script.
####################################################################################################
function mongoshell() {
    ./mongo --quiet --host ${MONGO_HOST} --port ${MONGO_PORT} --eval $@
}

####################################################################################################
# Check if the mongo node is primary
####################################################################################################
function is_primary_mongo() {
  echo $(mongoshell "rs.isMaster().ismaster")
}

####################################################################################################
# Wait for until primary mongo node ready
####################################################################################################
function wait_for_ready() {
  echo "Waiting for primary mongo node ready ..."
  while [ "$(is_primary_mongo)" != "true" ]; do sleep 3; done
  echo "Primary mongo node is now ready!"
  sleep 1
}

####################################################################################################
wait_for_ready && \
exec mongo-connector $@

